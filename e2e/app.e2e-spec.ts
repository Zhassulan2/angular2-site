import { AngularSite2Page } from './app.po';

describe('angular-site2 App', () => {
  let page: AngularSite2Page;

  beforeEach(() => {
    page = new AngularSite2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
