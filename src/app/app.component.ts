import { Component } from '@angular/core';
import { Hero } from './classes/hero.class';
import { HEROES } from './classes/HEROES.const';

@Component({
  selector: 'app-root',
  templateUrl: './component_templates/app.component.html',
  styleUrls: ['./resources/css/app.component.css']
})

export class AppComponent {
  title = 'Tour of Heroes';
  heroes = HEROES;
  selectedHero: Hero;
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }


}